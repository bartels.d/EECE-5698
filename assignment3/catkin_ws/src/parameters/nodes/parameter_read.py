#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def parameter_read():
    rospy.init_node('parameter_read')

    # Fetch values from the Parameter Server. In this example, we fetch
    # parameters from three different namespaces:
    #
    # 1) global (/global_param)
    # 2) parent (/foo/info)
    # 3) private (/foo/param_talker/topic_name)

    # fetch a /global parameter
    global_param = rospy.get_param("/global_param")
    rospy.loginfo("%s is %s", rospy.resolve_name('/global_param'), global_param)

    # fetch the utterance parameter from our parent namespace
    info = rospy.get_param('info')
    rospy.loginfo("%s is %s", rospy.resolve_name('info'), info)

    # fetch topic_name from the ~private namespace
    param_name = rospy.get_param('~param_name')
    rospy.loginfo("%s is %s", rospy.resolve_name('~param_name'), param_name)

    # fetch a parameter, using 'default_value' if it doesn't exist
    default_param = rospy.get_param('default_param', 'default_value')
    rospy.loginfo('%s is %s', rospy.resolve_name('default_param'), default_param)

    # fetch a group (dictionary) of parameters
    nums = rospy.get_param('nums')
    l, o, t = nums['L'], nums['O'], nums['T']
    rospy.loginfo("nums are %s, %s, %s", l, o, t)

    # set some parameters
    rospy.loginfo('setting parameters...')
    rospy.set_param('list_of_floats', [1., 2., 3., 4.])
    rospy.set_param('bool_True', True)
    rospy.set_param('~private_bar', 1+2)
    rospy.set_param('to_delete', 'baz')
    rospy.loginfo('...parameters have been set')

    # delete a parameter
    if rospy.has_param('to_delete'):
        rospy.delete_param('to_delete')
        rospy.loginfo("deleted %s parameter"%rospy.resolve_name('to_delete'))
    else:
        rospy.loginfo('parameter %s was already deleted'%rospy.resolve_name('to_delete'))

    # search for a parameter
    param_name = rospy.search_param('global_param')
    rospy.loginfo('found global_param parameter under key: %s'%param_name)

    # publish the value of utterance repeatedly
    pub = rospy.Publisher(param_name, String, queue_size=10)
    while not rospy.is_shutdown():
        pub.publish(info)
        rospy.loginfo(info)
        rospy.sleep(1)

if __name__ == '__main__':
    try:
        parameter_read()
    except rospy.ROSInterruptException: pass
