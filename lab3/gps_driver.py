#!/usr/bin/env python
# -*- coding: utf-8 -*-
# for BU-353s4 GPS Sensor

import sys
import lcm
import time
import serial
import utm
from my_types import gps_packet_t


class GPS(object):
    def __init__(self, port_name):
        self.port = serial.Serial(port_name, 4800, timeout=1.)  # 4800-N-8-1
        self.lcm = lcm.LCM("udpm://?ttl=12")
        self.packet = gps_packet_t()
        print 'GPS: Initialization'
        line = self.port.readline()
        try:
            value = [float(x) for x in line.split(' ')]
        except:
            value = 0

    def readloop(self):
        while True:
            line = self.port.readline()
            try:
                value = [x for x in line.split(',')]
                if value[0] == "$GPGGA":
                    print "Received Message..." + line
                    self.packet_from_response(value)
                    self.print_packet(self.packet)
                    self.lcm.publish("GPS Location", self.packet.encode())
            except Exception as e:
                print 'GPS ERROR (' + line + ')'
                print e

    def packet_from_response(self, value):
    #    Creates a gps_packet_t from a serial line response.
        def _extract_float(value):
            if value == "":
                return 0.0
            else:
                return float(value)

        self.packet.timestamp = _extract_float(value[1])
        self.packet.lat = _extract_float(value[2])
        self.packet.lon = _extract_float(value[4])
        self.packet.alt = _extract_float(value[9])

        if value[3] == 'S':
            self.packet.lat = -self.packet.lat;
        if value[5] == 'W':
            self.packet.lon = -self.packet.lon;

        try:
            utm_repr = utm.from_latlon(
                    _coord_to_decimal(self.packet.lat),
                    _coord_to_decimal(self.packet.lon))
            self.packet.utm_x = utm_repr[0]
            self.packet.utm_y = utm_repr[1]
        except Exception as e:
            print "Error trying to convert to utm: " + str(e)
            print "Lat: " + str(self.packet.lat)
            print "Lon: " + str(self.packet.lon)

    def print_packet(self, packet):
        print "GPS Packet:"
        print "***************"
        print "timestamp: " + str(packet.timestamp)
        print "lat: " + str(packet.lat)
        print "lon: " + str(packet.lon)
        print "alt: " + str(packet.alt)
        print "utm_x: " + str(packet.utm_x)
        print "utm_y: " + str(packet.utm_y)
        print "***************"
        print ""

def _coord_to_decimal(coord):
    degrees = coord//100
    decimal = (coord % 100)/60
    return degrees + decimal

if __name__ == "__main__":
    if len(sys.argv) == 1:
        gps = GPS("/dev/ttyACM0")
    elif len(sys.argv) != 2:
        print "Usage: %s <serial_port>\n" % sys.argv[0]
        sys.exit(0)
    else:
        gps = GPS(sys.argv[1])

    gps.readloop()
