clear

Array = csvread('/Users/bartelsd7369/Downloads/data/magnometer_calibration/imu_reading.csv');
MagX = Array(:,5)';
MagY = Array(:,6)';
MagZ = Array(:,7)';
AccelX = Array(:,8);
AccelY = Array(:,9);
AccelZ = Array(:,10);
GyroX = Array(:,11);
GyroY = Array(:,12);
GyroZ = Array(:,13);
Yaw = Array(:,2);
Pitch = Array(:,3);
Roll = Array(:,4);
i = 0;
t = Yaw;

%hard iron correction

MagXmin = min(MagX);
MagXmax = max(MagX);
MagYmin = min(MagY);
MagYmax = max(MagY);

alpha = (MagXmin + MagXmax)/2;
beta = (MagYmin + MagYmax)/2;
correctedX = MagX - alpha;
correctedY = MagY - beta;
figure(1)
%plot original magnetometer data
plot(MagX, MagY, 'r+')  
hold on
grid on
%plot hard iron corrected magnetometer data
plot(correctedX, correctedY, 'g*')
hold on
plot(0,0, 'b+')
title ('Hard Iron Correction for Magnetometer Data');
xlabel ('MagX');
ylabel ('MagY');
legend ('Original data','Corrected data');
axis([-0.4 0.4 -0.4 0.4]);

%soft iron correction
magnitude = zeros(size(MagX,2),3);
magnitude(:,2) = correctedX;
magnitude(:,3) = correctedY;
magnitude(:,1) = sqrt(magnitude(:,2).^2 + magnitude(:,3).^2);

r= max(magnitude(:,1));
q = abs(min(magnitude(:,2)));
max = find(magnitude(:,1) == r);
theta = asin(MagY(max) ./ r);

%rotate data
R = [cos(theta) sin(theta) ; -sin(theta) cos(theta)];
%v = rmagnitude;
v1 = R*(magnitude(:,2:3)');

%scale data
sigma = q / r;   
v1_scaled = v1./sigma;
v2 = [v1_scaled, v1(:,2)];

R1 = [cos(-theta) sin(-theta) ; -sin(-theta) cos(-theta)];
v3 = R1*v2;

figure(2)
plot (MagX, MagY, 'r+')
hold on
plot (v1(1,:), v1(2,:), 'b+')
hold on
plot ((v2(1,:)), (v2(2,:)), 'g+');
hold on
plot (v3(1,:), v3(2,:), 'c+');
title ('Soft Iron Correction for Magnetometer Data');
xlabel('MagX (gauss)');
ylabel('MagY (gauss)');
legend('Original Data', 'Rotated Data', 'Scaled Data', 'Final scaled data rotated back');


%yaw calculation on entire dataset
%hard iron correction
correctedXfull = MagXfull-alpha;
correctedYfull = MagYfull-beta;
s = [correctedXfull ; correctedYfull];
r1 = R1*s;

%soft iron correction
v1_scaled_full=r1(1,:)./sigma;

%yaw calculation from corrected magnetometer data 
yaw = atan2(r1(2,:),v1_scaled_full);

figure(3)
plot(t,(Yaw./(180/pi))+1,'r.');
hold on
plot(t,yaw, 'b.');
hold on 
xlabel('time (seconds)')
ylabel('Yaw angle (rad)')
title('Yaw angle from IMU and Magnetometer Data')
legend('Yaw angle from IMU', 'Yaw angle from Magnetometer')


% 1 pole butterworth low pass filter 

fc = 0.05;
fs = 40;
[b,a]=butter(1, fc/(fs/2));
%freqz(b,a);

dataIn = GyroZ;
dataOut = filter(b,a,dataIn);
yaw_angle = cumtrapz(dataOut);
yaw_angle = mod(yaw_angle,360);

figure(4)
plot(t, -yaw_angle+100, 'b.');
grid on
hold on 
plot(t, Yaw, 'r.');
title('Yaw Angle via Butterworth filter');
xlabel('Time (seconds)');
ylabel('Yaw Angle (degrees)');
legend('Yaw angle from Butterworth Filter', 'IMU yaw angle')


% cumtrapz yaw angle calculation
fsdivide= 1/40;
cumtrap_in=[];
cumtrap_in(1)=0;
cumtrap_in(2)=0;
cumtrap_in=cumtrapz(GyroZ);
cumtrap_in = mod(cumtrap_in,360);
figure(5)
plot(t,Yaw, 'r.')
hold on
plot(t,-cumtrap_in+100,'g.');
title('Yaw Angle via Cumtrapz filter');
xlabel('Time (seconds)');
ylabel('Yaw Angle (degrees)');
legend('IMU yaw angle', 'Yaw angle from cumtrapz')

%complementary filter
figure(6)
yaw_comp = 0.95.*yaw_angle+ 0.05.*cumtrap_in;
plot(t,Yaw,'b.');
hold on
plot(t,-yaw_comp+100,'r.');
title('Yaw angle from Complementary Filter and IMU');
xlabel('Time (sec)');
ylabel('Yaw angle (degrees)');
legend( 'IMU yaw angle','Complementary Filter Yaw angle');
%{
comp_yaw = [];
comp_yaw(1) = 0;
comp_yaw(2) = 0;


for i = 2:length(AccelY)
    comp_yaw(i) = 0.98*(comp_yaw(i-1) + cumtrap_in*fsdivide+0.02*Yaw(i));
    i=i+1;
end
%}


x = [];
x(1) = 0;
x(2) = 0;
%40 hz sampling rate
AccelX = AccelX - mean(AccelX);

%integrate for velocity
for i = 2:numel(AccelX)
    x(i) = trapz(t(1:i), AccelX(1:i));
end
x_position = [];
y_position = [];
ve = [];
vn = [];

%for i=1:length(x)
ve= x.*-cos(yaw_comp);
vn= x.*-sin(yaw_comp);
%end

for i=2:length(t)
    x_position(i) = trapz(t(1:i),ve(1:i));
    y_position(i) = trapz(t(1:i),vn(1:i));
end

figure(7)
plot(UtmX-mean(UtmX), UtmY-mean(UtmY), 'r.');
figure(8)
plot(-x_position./70, y_position./70, 'b.')
