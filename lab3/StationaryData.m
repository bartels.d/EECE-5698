clear;
Array = csvread('/Users/bartelsd7369/Downloads/data/2000PointsInPlaceInCar/imu_reading.csv');
magX = Array(:,5)';
magY = Array(:,6)';
magZ = Array(:,7)';
accX = Array(:,8)';
accY = Array(:,9)';
accZ = Array(:,10)';
gyroX = Array(:,11)';
gyroY = Array(:,12)';
gyroZ = Array(:,13)';
yawSensor = Array(:,2)';
pitch = Array(:,3)';
roll = Array(:,4)';
i = 0;

figure(1)
  subplot(3, 2, 1)
  plot(accX)
  title('Accel X')
  subplot(3, 2, 2)
  histogram(accX)  
  subplot(3, 2, 3)  
  plot(accY)
  title('Accel Y')
  subplot(3, 2, 4)
  histogram(accY)
  subplot(3, 2, 5)
  plot(accZ)
  title('Accel Z')
  subplot(3, 2, 6)
  histogram(accZ)
  
  figure(2)
  subplot(3, 2, 1)
  plot(magX)
  title('Mag X')
  subplot(3, 2, 2)
  histogram(magX)
  subplot(3, 2, 3)  
  plot(magY)
  title('Mag Y')
  subplot(3,2,4)
  histogram(magY)
  subplot(3, 2, 5)
  plot(magZ)
  title('Mag Z')
  subplot(3,2,6)
  histogram(magZ)
  
  figure(3)
  subplot(3, 2, 1)
  plot(gyroX)
  title('Gyro X')
  subplot(3, 2, 2)
  histogram(gyroX)
  subplot(3, 2, 3)  
  plot(gyroY)
  title('Gyro Y')
  subplot(3, 2, 4)
  histogram(gyroY)
  subplot(3, 2, 5)
  plot(gyroZ)
  title('Gyro Z')
  subplot(3, 2, 6)
  histogram(gyroZ)
  
  figure(4)
  subplot(3, 2, 1)
  plot(yaw)
  title('Yaw')
  subplot(3, 2, 2)
  histogram(yaw)
  subplot(3, 2, 3)  
  plot(pitch)
  title('Pitch')
  subplot(3, 2, 4)
  histogram(pitch)
  subplot(3, 2, 5)
  plot(roll)
  title('Roll Z')
  subplot(3, 2, 6)
  histogram(roll)
