#!/usr/bin/env python
# license removed for brevity

import rospy
import socket                  # Import socket module

from sensor_msgs.msg import NavSatFix

def publisher():
    # Create Publisher
    pub = rospy.Publisher('rtk_fix', NavSatFix, queue_size = 400)
    rospy.init_node('rtk_gps_node', anonymous=True)
    msg = NavSatFix()
    rate = rospy.Rate(10) # 10hz

    #Create Socket
    s = socket.socket()          # Create a socket object
    ip = '172.20.10.8'
    port = 3000
    host = rospy.get_param('~host', ip)  # these are parameters loaded onto the ros parameter servers
    port = rospy.get_param('~port', port)       # these are parameters loaded onto the ros parameter servers
    s.connect((host, port))
    print 'Connected to', ip , ' : ', port


    # Receive and publish
    try:
        while not rospy.is_shutdown():
            GPS = s.recv(1024)
            # GPS ='''GPGGA,1,4236.3213,12,07132.3213,49,321,321,1, 30
            # GDADS,3,2,12,13,12
            # DDSA,1,23,4,13
            # '''
            # GPSdata = GPS.split('\n')
            GPSdata = GPS.splitlines()
            for element in GPSdata:
                if(element.find('GPGGA') != -1):
                    data = element.split(',')
            # print data

            matchObj = re.search(r'(\d+)(\d\d\.\d+)', data[2])
            deg = float(matchObj.group(1))
            mins = float(matchObj.group(2))
            latitude = deg + (mins/60)
            if data[3] == 'S':
                latitude = -(latitude)
            print latitude
            msg.latitude = latitude
            #latitude = matchObj.group(1) + (matchObj.group(2)/60)
            matchObj = re.search(r'(\d+)(\d\d\.\d+)', data[4])
            deg = float(matchObj.group(1))
            mins = float(matchObj.group(2))
            longitude = deg + (mins/60)
            if data[5] == 'W':
                    longitude = -(longitude)
            msg.longitude = longitude
            msg.altitude = float(data[9])

            print msg
            # rospy.loginfo(msg)
            pub.publish(msg)
            rate.sleep()

    except rospy.ROSInterruptException:
        s.close()



if __name__ == '__main__':
    try:
        publisher()
    except rospy.ROSInterruptException:
        pass
