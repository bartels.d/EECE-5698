#!/usr/bin/env python
# license removed for brevity

import rospy
import utm
from sensor_msgs.msg import NavSatFix
from nav_msgs.msg import Odometry

def callback(data):
    # Create Publisher
    pub = rospy.Publisher('/utm_fix', Odometry)
    rospy.init_node('gps2utm_node', anonymous=True)
    msg = Odometry()
    rate = rospy.Rate(5) # 5hz
    

    # rospy.loginfo(rospy.get_caller_id() + 'I heard %s', data.data)

    try:
        lat = data.latitude
        longi = data.longitude
        alti= data.altitude


        utm_lat_lon = utm.from_latlon(lat, -longi)


        print lat
        print longi
        print utm_lat_lon[0]
        print utm_lat_lon[1]
        print alti
        print utm_lat_lon[2]
        print utm_lat_lon[3]


        msg.pose.pose.position.x = utm_lat_lon[0]
        msg.pose.pose.position.y = utm_lat_lon[1]
        msg.pose.pose.position.z = alti

        # print msg
        # rospy.loginfo(msg)
        pub.publish(msg)
        rate.sleep()
            

    except rospy.ROSInterruptException:
        pass



def subscriber():
    rospy.init_node('gps2utm_node', anonymous=True)
    while not rospy.is_shutdown():
        rospy.Subscriber('/rtk_fix', NavSatFix, callback)

        rospy.spin()

if __name__ == '__main__':
    try:
        subscriber()
    except rospy.ROSInterruptException:
        pass
