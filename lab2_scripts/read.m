clc;
clear;
filepath = '/home/dan/EECE-5698/lab2_scripts/clear_stationary.bag';
bag = rosbag(filepath);
bagselect2 = select(bag,'Topic', '/utm_fix');
allMsgs = readMessages(bagselect2);
datasize = size(allMsgs,1);

x = zeros(1, datasize);
y = zeros(1, datasize);
z = zeros(1, datasize);

for i = 1:datasize
    x(i) = allMsgs{i,1}.Pose.Pose.Position.X;
    y(i) = allMsgs{i,1}.Pose.Pose.Position.Y;
end

x = x-mean(x);
y = y-mean(y);


figure(1)
scatter(x,y,'b+')
title('utm_x vs utm_y open area stationary')
xlabel('utm_x - mean (m)')
ylabel('utm_y - mean (m)')
legend('observed track', 'real value')

figure(2)
subplot(2,1,1), histogram(x)
title('utm_x open area histogram')
xlabel('utm_x - mean (m)')
ylabel('Frequency')
%axis([-0.08 0.08 0 600])
subplot(2,1,2), histogram(y)
title('utm_y open area histogram')
xlabel('utm_y-mean (m)')
ylabel('Frequency')
%axis([-0.088 0.08 0 600])