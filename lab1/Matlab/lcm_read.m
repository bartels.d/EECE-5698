% add the lcm.jar file to the matlabpath - need to only do this once
javaaddpath /usr/local/share/java/lcm.jar

javaaddpath /home/dan/EECE-5698/lab1/python/my_types.jar


% Let’s assume the logging file is lcm-l.02 in the dir below
% open log file for reading

log_file = lcm.logging.Log('/home/dan/EECE-5698/lab1/python/log/lcm-log-2018-01-30-20:35:01', 'r'); 

% now read the file 
% here we are assuming that the channel we are interested in is RDI. Your channel 
% name will be different - something to do with GPS
% also RDI has fields altitude and ranges - GPS will probably have lat, lon, utmx,
% utmy etc
i=1;
while true
   try
        ev = log_file.readNext();
   % channel name is in ev.channel
   % there may be multiple channels but in this case you are only interested in RDI channel
       if strcmp(ev.channel, 'gps_channel')
 
     % build rdi object from data in this record
      msg = gps_package.gps_t(ev.data);
      % now you can do things like depending upon the rdi_t struct that was defined
      timestamp(i)=msg.timestamp;
      lat(i)=msg.lat;
      lon(i)=msg.lon;
      alt(i) = msg.altitude;
      utm_x(i) = msg.eastings;
      utm_y(i)= msg.northings;
       end
  catch ERR   % exception will be thrown when you hit end of file 
     break;
   end
   i=i+1;
end

figure
x = utm_x-mean(utm_x);
y = utm_y-mean(utm_y);
plot (utm_x, utm_y);
xlabel ('utm_x');
ylabel ('utm_y');
figure
plot (x,y);
xlabel ('utm_x minus mean');
ylabel ('utcm_y minus mean');

