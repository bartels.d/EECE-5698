import lcm

from gps_package import gps_t

def my_handler(channel, data):
	msg = gsp_t.decode(data)
	print("Received message on channel \"%s\"" % channel)
	print("	timestamp	= %s" % str(msg.timestamp))
	print("	latitude	= %s" % str(msg.lat))
	print(" latitude direction = %s" % str(msg.latdir))
	print("	longitude	= %s" % str(msg.lon))
	print(" longitude direction = %s" % str(msg.londir))
	print("	altitude	= %s" % str(msg.altitude))
	print("	eastings		= %s" % str(msg.eastings))
	print("	northings		= %s" % str(msg.northings))
	print("")

lc = lcm.LCM()
subscription = lc.subscribe("gps_channel", my_handler)

try:
	while True:
		lc.handle()
except KeyboardInterrupt:
	pass

lc.unsubscribe(subscription)
