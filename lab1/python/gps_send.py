import lcm
import time
import serial
import sys
import utm
import re
import numpy
from gps_package import gps_t

gpsdata = serial.Serial('/dev/ttyACM0',4800,timeout=4)

lc = lcm.LCM()


while True:

    info=gpsdata.readline()
    data_string=numpy.array(info.split(','))
    msg = gps_t()

    if data_string[0]=='$GPGGA':
        data_string[data_string=='']='0'
        print data_string
        msg.timestamp= float (data_string[1])
        msg.lat= str (data_string[2])
        msg.latdir = str (data_string[3])
        msg.lon= str (data_string[4])
        msg.londir= str (data_string[5])
        msg.altitude= float (data_string[9])
        print msg.lat
        print msg.lon
        #use regex to separate out the dd part of the lat/longitude
        matchObj = re.search(r'(\d+)(\d\d\.\d+)', msg.lat)
        deg = float(matchObj.group(1))
        mins = float(matchObj.group(2))
        latitude = deg + (mins/60)
        if msg.latdir == 'S':
            latitude = -(latitude)
        print latitude
        msg.lat = latitude
        #latitude = matchObj.group(1) + (matchObj.group(2)/60)
        matchObj = re.search(r'(\d+)(\d\d\.\d+)', msg.lon)
        deg = float(matchObj.group(1))
        mins = float(matchObj.group(2))
        longitude = deg + (mins/60)
        if msg.londir == 'W':
            longitude = -(longitude)
        print longitude
        msg.lon= longitude
        #longitude = matchObj.group(1) + (matchObj.group(2)/60)
        utmArr = utm.from_latlon(latitude, longitude)
        msg.eastings=float(utmArr[0])
        msg.northings=float(utmArr[1])
        lc.publish("gps_channel", msg.encode())
        pass
